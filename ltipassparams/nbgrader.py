
import nbgrader.auth
from .auth import MyLTI11Authenticator
class MyLTI11NBGraderAuthenticator(MyLTI11Authenticator, nbgrader.auth.Authenticator):
    pass
