import logging
import logging.config
import os
from typing import Optional

import psutil
import traitlets.config
from traitlets.utils.nested_update import nested_update


def get_configfile_from_cmdline() -> Optional[str]:
    """ Tries to find the JupyterHub configuration file, when the service is ran
        as a subprocess of JupyterHub. """
    cmdline = psutil.Process().parent().cmdline()
    try:
        i = cmdline.index("-f")
    except ValueError:
        return None
    if i == len(cmdline) - 1:
        return None
    return cmdline[i+1]


def get_jupyterhub_config():
    if getattr(get_jupyterhub_config, '_config', None) is not None:
        return get_jupyterhub_config._config

    """ Gets the LTI 1.1 consumer keys and secrets from the configuration file."""
    configfile = get_configfile()

    # Load the traitlets configuration
    app2 = traitlets.config.Application()
    app2.load_config_file(configfile)

    get_jupyterhub_config._config = app2.config
    return get_jupyterhub_config._config


def get_configfile() -> str:
    """ Finds the JupyterHub configuration file. """
    filename = get_configfile_from_cmdline()
    if filename:
        return filename

    if os.path.exists("/etc/jupyterhub/jupyterhub_config.py"):
        return "/etc/jupyterhub/jupyterhub_config.py"
    
    if os.path.exists("/opt/tljh/hub/lib/python3.8/site-packages/tljh/jupyterhub_config.py"):
        return "/opt/tljh/hub/lib/python3.8/site-packages/tljh/jupyterhub_config.py"

    raise FileNotFoundError("Could not find JupyterHub configuration file")


DEFAULT_LOGGING = {
    "version": 1,
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
            "formatter": "console",
            # "level": "DEBUG",
            "stream": "ext://sys.stderr",
        },
    },
    "formatters": {
        "console": {
            "class": (
                "traitlets.config.application.LevelFormatter"
            ),
            "format": '[%(levelname)1.1s %(asctime)s %(module)s:%(lineno)d] %(message)s',
            "datefmt": "%Y-%m-%d %H:%M:%S",
        },
    },
    "loggers": {
        "": {
            "level": "DEBUG",
            "handlers": ["console"],
        }
    },
    "disable_existing_loggers": False,
}


def setup_jupyterhub_logging():
    """ Sets up logging to be similar to that of JupyterHub. """
    c = get_jupyterhub_config()
    try:
        config = nested_update(DEFAULT_LOGGING, c.Application.logging_config)
    except TypeError:
        config = DEFAULT_LOGGING
    logging.config.dictConfig(config)
