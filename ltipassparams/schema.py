
# from dataclasses import dataclass
from typing import TypedDict

class LtiParamsDict(TypedDict):
    launch_presentation_return_url: str
    lti_message_type: str
    lti_version: str
    resource_link_id: str
    user_id: str
    roles: str
    lis_result_sourcedid: str
    context_id: str
    context_title: str
    context_label: str
    lis_outcome_service_url: str
    custom_next: str
    custom_component_display_name: str
    # ...

class SessionDict(TypedDict):
    checkout_root: str
    checkout_location: str
    oauth_consumer_key: str
    first_start_time: str
    last_start_time: str
    lti_params: LtiParamsDict


example = {
    "sessions": [
        {
            "checkout_root": "einfuehrung-python-data-scientists",
            "checkout_location": "einfuehrung-python-data-scientists/exercises/Kapitel 1 - Uebungen.ipynb",
            "oauth_consumer_key": "5fd68e27550971ef352762b91d55ab0cbb82179c163f8fa649f6b7b64e47d865",
            "lti_params": {
                "launch_presentation_return_url": "",
                "lti_message_type": "basic-lti-launch-request",
                "lti_version": "LTI-1p0",
                "resource_link_id": "edx.open-jupyter.gwdg.de-eb14a8799ccf4458891e5150611cee15",
                "user_id": "5b6c1c012f9ed3bd0e64e7262f686778",
                "roles": "Administrator",
                "lis_result_sourcedid": "course-v1%3AOpenJupyter%2BDS101%2BSoSe2023:edx.open-jupyter.gwdg.de-eb14a8799ccf4458891e5150611cee15:5b6c1c012f9ed3bd0e64e7262f686778",
                "context_id": "course-v1:OpenJupyter+DS101+SoSe2023",
                "context_title": "Introduction to Data Science",
                "context_label": "OpenJupyter",
                "lis_outcome_service_url": "https://edx.open-jupyter.gwdg.de/courses/course-v1:OpenJupyter+DS101+SoSe2023/xblock/block-v1:OpenJupyter+DS101+SoSe2023+type@lti_consumer+block@eb14a8799ccf4458891e5150611cee15/handler_noauth/outcome_service_handler",
                "custom_next": "https://c104-151.cloud.gwdg.de/hub/user-redirect/git-pull?repo=https%3A%2F%2Fgitlab-ce.gwdg.de%2Fpwieder%2Feinfuehrung-python-data-scientists&urlpath=tree%2Feinfuehrung-python-data-scientists%2Fexercises%2FKapitel+1+-+Uebungen.ipynb&branch=ST-2022",
                "custom_component_display_name": "Jupyter Notebook"
            }
        },
        {
            "checkout_root": "einfuehrung-python-data-scientists",
            "checkout_location": "einfuehrung-python-data-scientists/exercises/Kapitel 1 - Uebungen.ipynb",
            "oauth_consumer_key": "5fd68e27550971ef352762b91d55ab0cbb82179c163f8fa649f6b7b64e47d865",
            "lti_params": {
                "launch_presentation_return_url": "",
                "lti_message_type": "basic-lti-launch-request",
                "lti_version": "LTI-1p0",
                "resource_link_id": "edx.open-jupyter.gwdg.de-eb14a8799ccf4458891e5150611cee15",
                "user_id": "d177890a8ec87f974c11b3ff2bdd6940",
                "roles": "Student,Learner",
                "lis_result_sourcedid": "course-v1%3AOpenJupyter%2BDS101%2BSoSe2023:edx.open-jupyter.gwdg.de-eb14a8799ccf4458891e5150611cee15:d177890a8ec87f974c11b3ff2bdd6940",
                "context_id": "course-v1:OpenJupyter+DS101+SoSe2023",
                "context_title": "Introduction to Data Science",
                "context_label": "OpenJupyter",
                "lis_outcome_service_url": "https://edx.open-jupyter.gwdg.de/courses/course-v1:OpenJupyter+DS101+SoSe2023/xblock/block-v1:OpenJupyter+DS101+SoSe2023+type@lti_consumer+block@eb14a8799ccf4458891e5150611cee15/handler_noauth/outcome_service_handler",
                "custom_next": "https://c104-151.cloud.gwdg.de/hub/user-redirect/git-pull?repo=https%3A%2F%2Fgitlab-ce.gwdg.de%2Fpwieder%2Feinfuehrung-python-data-scientists&urlpath=tree%2Feinfuehrung-python-data-scientists%2Fexercises%2FKapitel+1+-+Uebungen.ipynb&branch=ST-2022",
                "custom_component_display_name": "Jupyter Notebook"
            }
        },
    ]
}